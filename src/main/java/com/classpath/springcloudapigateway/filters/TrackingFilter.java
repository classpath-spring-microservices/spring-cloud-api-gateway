package com.classpath.springcloudapigateway.filters;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
@Slf4j
@Order(1)
public class TrackingFilter implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        exchange.getRequest().mutate().header("request_id", UUID.randomUUID().toString());

        log.info("Inside the Filter :::: ");

        log.info("Request Id passed into the header ::: "+ exchange.getRequest().getHeaders().get("request_id"));
        return chain.filter(exchange);

    }
}